ncore@lithium:~/benchmarks/perlin$ env OMP_NUM_THREADS=1 ./perlin 
Compute Device Data
Device -- 10 Computes took 1.204064 seconds. Rate = 2.177160 Mpixels/sec
Compute Host Data
Host -- 10 Computes
computation time (in seconds): 1.203998
Mpixels/sec: 2.177280
0.999945 Speedup
Verifying....
Verification: Ok
ncore@lithium:~/benchmarks/perlin$ env OMP_NUM_THREADS=2 ./perlin 
Compute Device Data
Device -- 10 Computes took 0.602542 seconds. Rate = 4.350635 Mpixels/sec
Compute Host Data
Host -- 10 Computes
computation time (in seconds): 1.204004
Mpixels/sec: 2.177269
1.998208 Speedup
Verifying....
Verification: Ok
ncore@lithium:~/benchmarks/perlin$ env OMP_NUM_THREADS=4 ./perlin 
Compute Device Data
Device -- 10 Computes took 0.301646 seconds. Rate = 8.690452 Mpixels/sec
Compute Host Data
Host -- 10 Computes
computation time (in seconds): 1.204051
Mpixels/sec: 2.177183
3.991603 Speedup
Verifying....
Verification: Ok
