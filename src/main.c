/*-----------------------------------------------------------------------*/
/* Program: Perlin Noise                                                 */
/* Adapted to OmpSs by the Barcelona Supercomputing Center               */
/*************************************************************************/
/*                                                                       */
/* Licensed Materials - Property of IBM                                  */
/*                                                                       */
/*                                                                       */
/*                                                                       */
/* (C) Copyright IBM Corp. 2009                                          */
/* All Rights Reserved                                                   */
/*                                                                       */
/* US Government Users Restricted Rights - Use, duplication or           */
/* disclosure restricted by GSA ADP Schedule Contract with IBM Corp.     */
/*                                                                       */
/*************************************************************************/

/*************************************************************************/
/*                                                                       */
/* This sample is an OmpSs Perlin Noise generator, which is based on     */
/* Ken Perlin's Improved Noise implementation found at                   */
/* http://mrl.nyu.edu/~perlin/noise/. See readme.perlin_noise.txt for    */
/* more information.                                                     */
/*                                                                       */
/*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#ifdef CMD_LINE
#include <getopt.h>
#endif
#include <omp.h>

#include "perlin.h"

/**
 * process_command_line
 *
 * Parse the command line options and return the option values.
 */
#ifdef CMD_LINE
int process_command_line(int argc, char **argv, 
        int *data_size,
        int *iterations, int *once, int *verify,
        int *img_height,
        int *img_width, int *serial);
#endif

/**
 * main
 *
 * Compute perlin noise using OpenCL.
 */
int main(int argc, char **argv)
{
    int compute_serial = 1;

    int i;
    double delta;
    float time;
    //int iterations = 1500;
    int iterations = 10;
    float device_mpix;
    int verify = 1;
    int once = 1;
    int retval = 0;
    int data_size = MAX_DATA_SIZE;
    int img_width = MAX_IMG_WIDTH;
    int img_height = MAX_IMG_HEIGHT;

    pixel * output;

#ifdef CMD_LINE
    // Process command line arguments
    if (process_command_line
            (argc, argv, &data_size, &iterations, &once, &verify,
             &img_height, &img_width, &compute_serial) != 0) {
        fprintf(stderr, "Wrong parameters\n");
        return EXIT_FAILURE;
    }
#endif

    unsigned int rowstride = img_width;

    // Create the output array
    //
    output =(pixel*) calloc (img_width*img_height,sizeof(pixel));
    if (output==NULL) {
        perror ("malloc (output)");
        exit (1);
    }

    printf("OMP nthreads %d\n", omp_get_num_threads());
    printf("Compute Device Data\n");
    time = 0.0f;
    while (1) {

        double start = omp_get_wtime();

#if defined(__riscv_v)
       for (i = 0; i < iterations; i++) {            
            compute_perlin_noise_rvv(output, time, rowstride, img_height, img_width);
            time += 0.05f;
        }
#else
        for (i = 0; i < iterations; i++) {            
            compute_perlin_noise_smp(output, time, rowstride, img_height, img_width);
            time += 0.05f;
        }
#endif

        delta = omp_get_wtime() - start;

        if (once) {
            break;
        }

        if (delta < 5.0f) {
            iterations *= 2;
            time = 0.0f;
        } else {
            break;
        }
    }

    device_mpix =
        (double) (img_width * img_height) * (double) (iterations) / (1000000.0 * (double) (delta));

    if (compute_serial) {
        printf("Device -- %d Computes took %f seconds. Rate = %f Mpixels/sec\n",
                iterations, delta, device_mpix);
    } else {
        printf("Device -- %d Computes\n", iterations);
        printf("computation time (in seconds): %f\n", delta);
        printf("Mpixels/sec: %f\n", device_mpix);
    }

    retval = 0;

    if (compute_serial) {
        // Compute data on host and verify device results
        //
        retval = compute_host_and_verify(iterations, output, rowstride,
                img_height, img_width, verify, data_size,
                device_mpix);
    }

    free(output);

    return retval;
}

#ifdef CMD_LINE
int process_command_line(int argc, char **argv,
        int *data_size,
        int *iterations, int *once, int *verify,
        int *img_height,
        int *img_width, int *serial)
{
    int opt, dimension;

    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"help", 0, NULL, 'h'},
            {"dim", 1, NULL, 'd'},
            {"iter", 1, NULL, 'i'},
            {"noverify", 0, NULL, 'n'},
            {"once", 0, NULL, 'o'},
            {"serial", 0, NULL, 't'},
            {NULL, 0, NULL, 0}
        };

        opt =
            getopt_long(argc, argv, "hd:i:no", long_options, &option_index);

        if (opt == -1) {
            break;
        }

        switch (opt) {
            case 'h':
            case '?':
                printf("Usage: %s [--help] [DEVICE] [KERNELTYPE] [OPTIONS...]\n",
                        argv[0]);
                printf("\n");
                printf(" Options:\n");
                printf("\n");
                printf
                    (" -d, --dim N           image dimension (power of 2, between 4 and 1024)\n");
                printf("                       (default 1024)\n");
                printf
                    (" -i, --iter N          number of iterations each pass (default: 100)\n");
                printf
                    (" -n, --noverify        don't check device output with host computed\n");
                printf("                       (default: verify)\n");
                printf
                    (" -o, --once            only do 1 pass, don't try to fill up 5.0 seconds\n");
                printf("                       (default: fill up 5.0 seconds)\n");
                printf
                    (" --serial              compute the serial base time and check result from GPU\n");
                printf("                       (default: 0)\n");

                exit(EXIT_SUCCESS);
                break;
            case 'd':
                dimension = strtoul(optarg, 0, 0);
                switch (dimension) {
                    case 4:
                    case 8:
                    case 16:
                    case 32:
                    case 64:
                    case 128:
                    case 256:
                    case 512:
                    case 1024:
                        break;
                    default:
                        fprintf(stderr,
                                "Error, %d is not a valid power of 2 between 4 and 1024\n",
                                dimension);
                        return 1;
                }
                *img_width = dimension;
                *img_height = dimension;
                *data_size = (*img_width) * (*img_height) * 4;
                break;
            case 'i':
                *iterations = strtoul(optarg, 0, 0);
                break;
            case 'n':
                *verify = 0;
                break;
            case 'o':
                *once = 1;
                break;
            case 't':
                *serial = 1;
                break;
            default:
                return 1;
        }
    }

    return 0;
}
#endif

/**
 * compute_host_and_verify
 *
 * Perform the host-side perlin noise computation, report performance and optionally verify the device results.
 */
int compute_host_and_verify(int iterations, pixel * output_device, int rowstride,
        int img_height, int img_width, int verify, int data_size, float device_mpix)
{
    int retval = 0;
    int i;
    float host_mpix;
    double delta;
    float time;
    pixel *output;

    if (verify) {

        printf("Compute Host Data\n");
        // Create the output array

        output =(pixel*) calloc (img_width*img_height,sizeof(pixel));
        if (output==NULL) {
            perror ("malloc (output)");
            exit (1);
        }

        time = 0.0f;
        double start = omp_get_wtime();

        for (i = 0; i < iterations; i++) {
            compute_perlin_noise((pixel *) output, time, rowstride, img_height, img_width);
            time += 0.05f;
        }

        delta = omp_get_wtime() - start;


        host_mpix = (double) (img_width * img_height) * (double) (iterations) / (1000000.0 * (double) (delta));
        printf("Host -- %d Computes\n", iterations);
        printf("computation time (in seconds): %f\n", delta);
        printf("Mpixels/sec: %f\n", host_mpix);
        printf("%f Speedup\n", device_mpix / host_mpix);


        // Verify results
        //
        printf("Verifying....\n");

        int failure_count = 0;
        for (i = 0; i < img_width * img_height ; i++) {
            pixel *device_pixel = (pixel *) & output_device[i];
            pixel *host_pixel = (pixel *) & output[i];

            //The rgb values may be off by 1 but still be correct because of rounding error.
            //For example, if the host computes 164.000015 and the device 163.999985,
            //the resulting values will be 164 and 163, respectively.

            if ((abs(device_pixel->r - host_pixel->r) > 1) ||
                    (abs(device_pixel->g - host_pixel->g) > 1) ||
                    (abs(device_pixel->b - host_pixel->b) > 1) ||
                    (abs(device_pixel->a - host_pixel->a) > 1)) {
                retval = 1;
                printf("Error [%d]: H 0x%08X 0x%08X\n", i, output[i], output_device[i]);
                failure_count++;
                if (failure_count>=10) {
                    printf("stopping after 10 errors\n");
                    break;
                }
            }
        }
    
        if (failure_count) {
            printf("Verification Complete: %d errors out of %d pixels\n", failure_count, data_size / sizeof(pixel));
        } else {
            printf("Verification: Ok\n");
        }
    }

    return retval; 
}
