/*************************************************************************/
/*                                                                       */
/* Licensed Materials - Property of IBM                                  */
/*                                                                       */
/*                                                                       */
/*                                                                       */
/* (C) Copyright IBM Corp. 2009                                          */
/*                                                                       */
/*************************************************************************/

#ifndef PERLIN_H_
#define PERLIN_H_

//#include <sys/types.h>
#include <stdint.h>

#define   MAX_IMG_WIDTH     512
#define   MAX_IMG_HEIGHT    512
#define   MAX_DATA_SIZE     (MAX_IMG_WIDTH * MAX_IMG_HEIGHT * 4)

typedef struct colors {
  uint8_t r;
  uint8_t g;
  uint8_t b;
  uint8_t a;
} pixel;

int compute_host_and_verify(int iterations, pixel * output_device, int rowstride,
                            int img_height, int img_width, int verify, int data_size, float device_mpix);
                            
void compute_perlin_noise_rvv(pixel *output, const float time, const uint32_t rowstride, uint32_t img_height, uint32_t img_width);

void compute_perlin_noise_smp(pixel * output, float time, unsigned int rowstride, int img_height, int img_width);                            

void compute_perlin_noise(pixel * output, const float time, const unsigned int rowstride, int img_height, int img_width);

double get_wtime();
#define omp_get_wtime get_wtime


#endif /*PERLIN_H_ */
