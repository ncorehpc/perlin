
// Implementation courtesy SiFive

#include "perlin.h"
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <fenv.h>

#if defined(__riscv_v)
#include <riscv_vector.h>

// The original size of lookup table is 256.
// We use vluxseg2ei8 to load two elements in one call. So, we should handle
// the `index == 255 case`. Just extend one more element here.
static uint8_t perm_u8[257] = {
    151, 160, 137, 91,  90,  15,  131, 13,  201, 95,  96,  53,  194, 233, 7,
    225, 140, 36,  103, 30,  69,  142, 8,   99,  37,  240, 21,  10,  23,  190,
    6,   148, 247, 120, 234, 75,  0,   26,  197, 62,  94,  252, 219, 203, 117,
    35,  11,  32,  57,  177, 33,  88,  237, 149, 56,  87,  174, 20,  125, 136,
    171, 168, 68,  175, 74,  165, 71,  134, 139, 48,  27,  166, 77,  146, 158,
    231, 83,  111, 229, 122, 60,  211, 133, 230, 220, 105, 92,  41,  55,  46,
    245, 40,  244, 102, 143, 54,  65,  25,  63,  161, 1,   216, 80,  73,  209,
    76,  132, 187, 208, 89,  18,  169, 200, 196, 135, 130, 116, 188, 159, 86,
    164, 100, 109, 198, 173, 186, 3,   64,  52,  217, 226, 250, 124, 123, 5,
    202, 38,  147, 118, 126, 255, 82,  85,  212, 207, 206, 59,  227, 47,  16,
    58,  17,  182, 189, 28,  42,  223, 183, 170, 213, 119, 248, 152, 2,   44,
    154, 163, 70,  221, 153, 101, 155, 167, 43,  172, 9,   129, 22,  39,  253,
    19,  98,  108, 110, 79,  113, 224, 232, 178, 185, 112, 104, 218, 246, 97,
    228, 251, 34,  242, 193, 238, 210, 144, 12,  191, 179, 162, 241, 81,  51,
    145, 235, 249, 14,  239, 107, 49,  192, 214, 31,  181, 199, 106, 157, 184,
    84,  204, 176, 115, 121, 50,  45,  127, 4,   150, 254, 138, 236, 205, 93,
    222, 114, 67,  29,  24,  72,  243, 141, 128, 195, 78,  66,  215, 61,  156,
    180,

    151,
};

inline vfloat32m1_t grad_rvv(vuint8mf4_t hash, vfloat32m1_t x, vfloat32m1_t y,
                             vfloat32m1_t z, size_t vl) 
{
  vuint8mf4_t hash13 = vand(hash, 13, vl);

  // if h < 8 then x, else y
  vfloat32m1_t u = vmerge(vmsltu(hash13, 8, vl), y, x, vl);

  // if h < 4 then y else if h is 12 or 14 then x else z
  vfloat32m1_t v = vmerge(vmseq(hash13, 12, vl), z, x, vl);
  v = vmerge(vmsltu(hash13, 2, vl), v, y, vl);

  // if h & 1 then -u else u
  // if h & 2 then -v else v
  vuint8mf4_t hash1 = vand(hash, 1, vl);
  u = vfsgnjn(vmsne(hash1, 0, vl), u, u, u, vl);
  vuint8mf4_t hash2 = vand(hash, 2, vl);
  v = vfsgnjn(vmsne(hash2, 0, vl), v, v, v, vl);

  return vfadd(u, v, vl);
}

inline vfloat32m1_t lerp_rvv(vfloat32m1_t t, vfloat32m1_t a, vfloat32m1_t b,
                             size_t vl) 
{
  // return a + t * (b - a);
  vfloat32m1_t temp = vfsub(b, a, vl);
  temp = vfmadd(temp, t, a, vl);

  return temp;
}

inline vfloat32m1_t fade_rvv(vfloat32m1_t in, size_t vl) 
{
  // return t * t * t * (t * (t * 6.0f - 15.0f) + 10.0f);
  vfloat32m1_t temp = vfmul(in, 6.0f, vl);
  temp = vfsub(temp, 15.0f, vl);
  temp = vfmul(temp, in, vl);
  temp = vfadd(temp, 10.0f, vl);
  temp = vfmul(temp, in, vl);
  temp = vfmul(temp, in, vl);
  temp = vfmul(temp, in, vl);

  return temp;
}

inline vfloat32m1_t noise3_rvv(vfloat32m1_t x_vec, vfloat32m1_t y_vec,
                               vfloat32m1_t z_vec, size_t vl,
                               float clip_value) 
{
  vfloat32m1_t floor_x_vec = vfcvt_f(vfcvt_x(x_vec, vl), vl);
  vfloat32m1_t floor_y_vec = vfcvt_f(vfcvt_x(y_vec, vl), vl);
  vfloat32m1_t floor_z_vec = vfcvt_f(vfcvt_x(z_vec, vl), vl);

  x_vec = vfsub(x_vec, floor_x_vec, vl);
  y_vec = vfsub(y_vec, floor_y_vec, vl);
  z_vec = vfsub(z_vec, floor_z_vec, vl);

  vfloat32m1_t u_vec = fade_rvv(x_vec, vl);
  vfloat32m1_t v_vec = fade_rvv(y_vec, vl);
  vfloat32m1_t w_vec = fade_rvv(z_vec, vl);

  vfloat32m1_t x1_vec = vfsub(x_vec, 1.0f, vl);
  vfloat32m1_t y1_vec = vfsub(y_vec, 1.0f, vl);
  vfloat32m1_t z1_vec = vfsub(z_vec, 1.0f, vl);

  vuint8mf4_t X_vec = sf_vfnrclip_xu_f_qf_u8mf4(floor_x_vec, clip_value, vl);
  vuint8mf4_t Y_vec = sf_vfnrclip_xu_f_qf_u8mf4(floor_y_vec, clip_value, vl);
  vuint8mf4_t Z_vec = sf_vfnrclip_xu_f_qf_u8mf4(floor_z_vec, clip_value, vl);

  vuint8mf4_t A_vec;
  vuint8mf4_t B_vec;
  vluxseg2ei8(&A_vec, &B_vec, perm_u8, X_vec, vl);
  A_vec = vadd(A_vec, Y_vec, vl);
  B_vec = vadd(B_vec, Y_vec, vl);

  vuint8mf4_t AA_vec;
  vuint8mf4_t AB_vec;
  vluxseg2ei8(&AA_vec, &AB_vec, perm_u8, A_vec, vl);
  AA_vec = vadd(AA_vec, Z_vec, vl);
  AB_vec = vadd(AB_vec, Z_vec, vl);

  vuint8mf4_t BA_vec;
  vuint8mf4_t BB_vec;
  vluxseg2ei8(&BA_vec, &BB_vec, perm_u8, B_vec, vl);
  BA_vec = vadd(BA_vec, Z_vec, vl);
  BB_vec = vadd(BB_vec, Z_vec, vl);

  vuint8mf4_t g0_perm_vec;
  vuint8mf4_t g4_perm_vec;
  vluxseg2ei8(&g0_perm_vec, &g4_perm_vec, perm_u8, AA_vec, vl);

  vuint8mf4_t g1_perm_vec;
  vuint8mf4_t g5_perm_vec;
  vluxseg2ei8(&g1_perm_vec, &g5_perm_vec, perm_u8, BA_vec, vl);

  vuint8mf4_t g2_perm_vec;
  vuint8mf4_t g6_perm_vec;
  vluxseg2ei8(&g2_perm_vec, &g6_perm_vec, perm_u8, AB_vec, vl);

  vuint8mf4_t g3_perm_vec;
  vuint8mf4_t g7_perm_vec;
  vluxseg2ei8(&g3_perm_vec, &g7_perm_vec, perm_u8, BB_vec, vl);

  vfloat32m1_t g0_vec = grad_rvv(g0_perm_vec, x_vec, y_vec, z_vec, vl);
  vfloat32m1_t g4_vec = grad_rvv(g4_perm_vec, x_vec, y_vec, z1_vec, vl);
  vfloat32m1_t g1_vec = grad_rvv(g1_perm_vec, x1_vec, y_vec, z_vec, vl);
  vfloat32m1_t g5_vec = grad_rvv(g5_perm_vec, x1_vec, y_vec, z1_vec, vl);
  vfloat32m1_t g2_vec = grad_rvv(g2_perm_vec, x_vec, y1_vec, z_vec, vl);
  vfloat32m1_t g6_vec = grad_rvv(g6_perm_vec, x_vec, y1_vec, z1_vec, vl);
  vfloat32m1_t g3_vec = grad_rvv(g3_perm_vec, x1_vec, y1_vec, z_vec, vl);
  vfloat32m1_t g7_vec = grad_rvv(g7_perm_vec, x1_vec, y1_vec, z1_vec, vl);

  vfloat32m1_t u01_vec = lerp_rvv(u_vec, g0_vec, g1_vec, vl);
  vfloat32m1_t u23_vec = lerp_rvv(u_vec, g2_vec, g3_vec, vl);
  vfloat32m1_t u45_vec = lerp_rvv(u_vec, g4_vec, g5_vec, vl);
  vfloat32m1_t u67_vec = lerp_rvv(u_vec, g6_vec, g7_vec, vl);

  vfloat32m1_t v0_vec = lerp_rvv(v_vec, u01_vec, u23_vec, vl);
  vfloat32m1_t v1_vec = lerp_rvv(v_vec, u45_vec, u67_vec, vl);

  return lerp_rvv(w_vec, v0_vec, v1_vec, vl);
}

void compute_perlin_noise_rvv(pixel *output, const float time,
                             const uint32_t rowstride, uint32_t img_height,
                             uint32_t img_width) 
{
#if 0
  // We could warm up the L1 cache for the perm-look-up-table using scalar load.
  volatile uint8_t temp;
  for (int i = 0; i < 256; ++i) {
    temp += perm_u8[i];
  }
#endif

  // We assume that image data could be access without stride.
  assert(rowstride == img_width);

  // TODO: (Jerry) find a proper rounding mode.
  int old_round_mode = fegetround();
  fesetround(FE_DOWNWARD);

  uint8_t *output_ptr = (uint8_t *)output;

  // Vectorize per row.
  const float vdx = 0.03125f;
  const float vdy = 0.0125f;
  const float vs = 2.0f;
  const float bias = 0.35f;

  vuint8mf4_t r_vec;
  vuint8mf4_t g_vec;
  vuint8mf4_t b_vec;
  vuint8mf4_t a_vec;

  union {
    float f;
    uint32_t i;
  } clip_value;
  clip_value.i = 255;

  size_t a_vl = vsetvl_e8mf4(img_width);
  a_vec = vmv_v_x_u8mf4(255, a_vl);

  // vt = time * vs;
  size_t vt_vl = vsetvl_e32m1(img_width);
  vfloat32m1_t vt_vec = vfmv_v_f_f32m1(time * vs, vt_vl);

  size_t id_vl = vsetvl_e16mf2(img_width);
  // Assume (vl<65536), so we use e16 for id.
  assert(id_vl < 65536);
  vuint16mf2_t id_vec = vid_v_u16mf2(id_vl);
  vfloat32m1_t row_id_vec = vfwcvt_f(id_vec, id_vl);

  uint32_t i, j;

  #pragma omp parallel for shared (perm_u8, output, img_width) \
                             firstprivate (vs, bias, vdx, vdy) \
                             private (i, j)
  for (i = 0; i < img_height; ++i) {
    // yy = ((float)row_index) * vdy * vs;
    size_t yy_vl = vsetvl_e32m1(img_width);
    vfloat32m1_t yy_vec = vfmv_v_f_f32m1(i * vdy * vs, yy_vl);

    size_t xx_vl;
    uint32_t width = img_width;
    for (j = 0; j < img_width; width -= xx_vl, j += xx_vl) {
      // xx = ((float)column_index) * vdx * vs;
      xx_vl = vsetvl_e32m1(width);
      vfloat32m1_t xx_vec = vfadd(row_id_vec, j, xx_vl);
      xx_vec = vfmul(xx_vec, vdx * vs, xx_vl);

      vfloat32m1_t noise_r_vec =
          noise3_rvv(xx_vec, vt_vec, yy_vec, xx_vl, clip_value.f);
      noise_r_vec = vfadd(noise_r_vec, bias, xx_vl);
      noise_r_vec = vfmul(noise_r_vec, 255.0f, xx_vl);
      r_vec = sf_vfnrclip_xu_f_qf_u8mf4(noise_r_vec, clip_value.f, xx_vl);

      vfloat32m1_t noise_g_vec =
          noise3_rvv(vt_vec, yy_vec, xx_vec, xx_vl, clip_value.f);
      noise_g_vec = vfadd(noise_g_vec, bias, xx_vl);
      noise_g_vec = vfmul(noise_g_vec, 255.0f, xx_vl);
      g_vec = sf_vfnrclip_xu_f_qf_u8mf4(noise_g_vec, clip_value.f, xx_vl);

      vfloat32m1_t noise_b_vec =
          noise3_rvv(yy_vec, xx_vec, vt_vec, xx_vl, clip_value.f);
      noise_b_vec = vfadd(noise_b_vec, bias, xx_vl);
      noise_b_vec = vfmul(noise_b_vec, 255.0f, xx_vl);
      b_vec = sf_vfnrclip_xu_f_qf_u8mf4(noise_b_vec, clip_value.f, xx_vl);

      vsseg4e8(output_ptr, r_vec, g_vec, b_vec, a_vec, xx_vl);
      output_ptr += 4 * xx_vl;
    }
  }

  fesetround(old_round_mode);

}

#endif // (__riscv_v)