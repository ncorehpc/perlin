
#ifdef __TMS320C6X__
static int firstcall = 1; 
#include <c6x.h>
#else
#include <sys/time.h>
#endif

/* Time in seconds */
double get_wtime()
{
#ifdef __TMS320C6X__
      if (firstcall)
      {
          firstcall = 0;
          TSCL = 0;
          TSCH = 0;
      }

      return (double)_itoll(TSCH, TSCL) * 1.0e-9;
#else
      struct timeval t;
      gettimeofday(&t, 0);

      return t.tv_sec + t.tv_usec/1000000.0;
#endif
}

