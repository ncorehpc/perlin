# perlin noise benchmark

This is benchmark computes perlin noise. It uses vectorization and openmp.

# License
```

/*-----------------------------------------------------------------------*/
/* Program: Perlin Noise                                                 */
/* Adapted to OmpSs by the Barcelona Supercomputing Center               */
/*************************************************************************/
/*                                                                       */
/* Licensed Materials - Property of IBM                                  */
/*                                                                       */
/* Created by Mark Nutter on 1/28/09.                                    */
/* (C) Copyright IBM Corp. 2009                                          */
/*                                                                       */
/* Derived from Improved Noise (http://mrl.nyu.edu/~perlin/noise/)       */
/* (C) Copyright Ken Perlin 2002                                         */
/*                                                                       */
/*************************************************************************/

```


# Build

`make` makes on x86
`make TARGET=<arch>` to cross compile on x86 where target is:

`aarch64`
`x280`

`make OPENMP=1`

builds an openmp enabled version

# Collecting performance data

`./run_perfstats` collects benchmark information on Linux via `perf`.
