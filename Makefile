ARCH=$(shell echo `uname -m`)

ifeq ($(ARCH),aarch64)
include make.aarch64
 CC=gcc
endif

ifeq ($(ARCH),riscv64)
include make.riscv64
endif

ifeq ($(ARCH),x86_64)
include make.cross
endif

#OBJECTS+=src/getclock.c src/perlin.c src/main.c
OBJECTS+=src/getclock.c src/perlin_rvv.c src/perlin.c src/main.c
perlin: build_vars
	$(CC) $(OPT) $(FLAGS) $(DEFINES) $(OBJECTS) -o $@ $(LFLAGS)

build_vars:
	@echo "COMPILER:$(CC)" > build_vars.txt
	@echo "CFLAGS:$(OPT)" >> build_vars.txt

lst:
	$(LST) --source --all-headers --demangle --line-numbers --wide perlin > perlin.lst

clean:
	rm -f perlin build_vars.txt

run:
	./perlin
